package dev.mbkaue.arvorebinaria;

import java.util.Random;

/**
 *
 * @author mbkau
 */
public class ArvoreBinaria {

    public static void main(String[] args) {

        BinaryTree tree = new BinaryTree();

        for (int i = 0; i < 15; i++) {
            int novoValor = new Random().nextInt(50);
            tree.inserir(novoValor);
        }
        
        tree.exibir(tree.raiz);
    }
}
