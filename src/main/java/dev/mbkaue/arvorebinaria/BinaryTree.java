package dev.mbkaue.arvorebinaria;

/**
 *
 * @author mbkau
 */
public class BinaryTree {

    static NoA raiz;

    public static void exibir(NoA temp) {
        if (temp != null) {
            exibir(temp.esq);
            System.out.println(temp.valor);
            exibir(temp.dir);
        }
    }

    public static void inserir(int x) {
        NoA novo = new NoA(x);

        if (raiz == null) {
            raiz = novo;
        } else {
            NoA temp = raiz;
            boolean inseriu = false;

            while (!inseriu) {
                if (novo.valor <= temp.valor) {
                    if (temp.esq == null) {
                        temp.esq = novo;
                        inseriu = true;
                    } else {
                        temp = temp.esq;
                    }
                } else {
                    if (temp.dir == null) {
                        temp.dir = novo;
                        inseriu = true;
                    } else {
                        temp = temp.dir;
                    }
                }
            }
        }
    }
}
